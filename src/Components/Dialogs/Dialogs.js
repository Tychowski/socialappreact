import React from 'react'
import s from './dialogs.module.css'
import DialogItem from './DialogItem/DialogItem'
import Message from './Message/Message'
import { Redirect } from 'react-router-dom'
import { Field, reduxForm } from 'redux-form'
import { Textarea } from '../common/FormsControls/FormsControls'
import { required, maxLengthCreator } from '../../utils/validators/validotors'


const Dialogs = (props) => {

    let state = props.dialogsPage;
    
    let dialogsElements = state.messages.map( (d) => <DialogItem id={d.id} key={d.id} name={d.name} /> );
    let messagesElements = state.dialogs.map( (m) => <Message id={m.id} key={m.id} message={m.message} /> );
    let newMessageBody = state.newMessageBody;

    let addNewMessage = (values) => {
        props.sendMessage(values.newMessageBody);
    }

    if (!props.isAuth) return <Redirect to={"/login"} />

    return (
        <div className={s.dialogs}>
            <div className={s.dialogsItems}>
                { dialogsElements }
            </div>
            <div className={s.messages}>
               <div>{ messagesElements }</div> 
            </div>
            <ReduxMessageForm onSubmit={addNewMessage} />
        </div>
    )
}

const maxLength50 = maxLengthCreator(50);

const AddMessageForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field component={Textarea} validate={[required, maxLength50]}
                name={'newMessageBody'} placeholder={'Enter your message'} /> 
            </div>
            <div><button>Send</button></div>
        </form>
    )
}

const ReduxMessageForm = reduxForm({form: 'DialogsAddMessageForm'})(AddMessageForm);

export default Dialogs