import React from 'react'
import Dialogs from './Dialogs'
import {sendMessageCreactor, updateNewMessageBodyCreator} from '../../Redux/dialogs-reducer'
import { connect } from 'react-redux'
import { withAuthRedirect } from '../../Hoc/withAuthRedirect'
import { compose } from 'redux'



let mapStateToProps = (state) => {
    return {
        dialogsPage: state.dialogsPage
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        sendMessage: (newMessageBody) => {
            dispatch(sendMessageCreactor(newMessageBody))
        },
        updateNewMessageBody: (body) => {
            dispatch(updateNewMessageBodyCreator(body))
        }
    }
}


export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withAuthRedirect
)(Dialogs)