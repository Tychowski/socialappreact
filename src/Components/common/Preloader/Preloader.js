import React from 'react'
import s from '../../Users/users.module.css'

let spinner = "https://gifimage.net/wp-content/uploads/2017/08/spinner-gif-16.gif";

let Preloader = (props) => {
    return <img className={s.spinner} src={spinner} />
}

export default Preloader