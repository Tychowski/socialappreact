import React from 'react'
import style from './FormControls.module.css'

const FormControl = ({input, meta, ...props}) => {

    const hasError = meta.touched && meta.error;

    return (
        <div className={style.formControl + " " + (hasError  ? style.error : "") }>
            <div>
                {props.children}
            </div>
            <div>
                { hasError && <span>{meta.error}</span> }
            </div>
        </div>
    )
}

export const Textarea = (props) => {
    const {input, meta, child, ...Restprops} = props;
    return <FormControl {...props}><textarea {...input} {...Restprops} /></FormControl>
}

export const Input = (props) => {
    const {input, meta, child, ...Restprops} = props;
    return <FormControl {...props}><input {...input} {...Restprops} /></FormControl>
}