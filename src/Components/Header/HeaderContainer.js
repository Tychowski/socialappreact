import React, { Component } from 'react'
import Header from './Header'
import { connect } from 'react-redux'
import { logout } from '../../Redux/auth-reducer'


class HeaderContainer extends Component {

    render() {
        return <Header {...this.props} />
    }
}

const mapsStateToProps = (state) => ({
    isAuth: state.auth.isAuth,
    login: state.auth.login
    // logout: state.auth.logout
});

export default connect(mapsStateToProps, {logout}) (HeaderContainer);