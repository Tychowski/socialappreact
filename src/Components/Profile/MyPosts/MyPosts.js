import React from 'react'
import s from './mypost.module.css'
import Post from './Post/Posts' 
import { addPostActionCreator, updateNewPostActionCreator } from '../../../Redux/profile-reducer'
import { Field, reduxForm } from 'redux-form'
import { required, maxLengthCreator } from '../../../utils/validators/validotors'
import { Textarea } from '../../common/FormsControls/FormsControls'


const MyPosts = (props) => {

    let postsElements = 
        [...props.posts]
            .reverse()
            .map( (p) => <Post message={p.message} likeCounts={p.liksCount} /> )

    let newPostElement = React.createRef();

    let onAddPost = (values) => {
        props.addPost(values.newPostText); 
    }

    return( 
        <div className={s.postBlock}>
            <h3>My posts</h3>
            <ReduxPostForm onSubmit={onAddPost} />
            <br />
            <div className={s.posts}>
                { postsElements }
            </div>
        </div>
    )
}

const maxLength10 = maxLengthCreator(10);

const AddPost = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field component={Textarea} name={'newPostText'} placeholder={'Add post'} 
                       validate={[required, maxLength10]}
                />
            </div>
            <div>
                <button>Add post</button>
            </div>
        </form>
    )
}

const ReduxPostForm = reduxForm({form: 'ProfileAddPost '})(AddPost);

export default MyPosts












// import React from 'react'
// import s from './mypost.module.css'
// import Post from './Post/Posts' 
// import { addPostActionCreator, updateNewPostActionCreator } from '../../../Redux/profile-reducer'


// const MyPosts =  (props) => {
//     let postsElements = props.posts.map( (p) => <Post message={p.message} likeCounts={p.liksCount} /> )

//     let newPostElement = React.createRef();

//     let addPost = () => {
//         props.dispatch(addPostActionCreator())
//     }

//     let onPostChange = () => {
//         let text = newPostElement.current.value;
//         props.dispatch(updateNewPostActionCreator(text));
//     }

//     return( 
//         <div className={s.postBlock}>
//             <h3>My posts</h3>
//             <div>
//                 <div>
//                     <textarea onChange={onPostChange} ref={ newPostElement } value={props.newPostText} />
//                 </div>
//                 <div>
//                     <button onClick={ addPost }>Add post</button>
//                 </div>
//             </div>
//             <br />
//             <div className={s.posts}>
//                 { postsElements }
//             </div>
//         </div>
//     )
// }

// export default MyPosts