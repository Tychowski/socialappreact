import React from 'react'
import s from './post.module.css'

const Posts = (props) => {

    return( 
        <div className={s.item}>
            <img src="http://i.pinimg.com/474x/5a/1b/1e/5a1b1e32639c6210416804dc7a93ef8e--minecraft-pixel-art-superman.jpg"></img>
            {props.message}
            <div>
                <span>{props.likeCounts}</span>
            </div>
        </div>
    
    )
}

export default Posts