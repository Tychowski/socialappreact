import React from "react"
import { create } from "react-test-renderer"
import ProfileStatus from './ProfileStatus'

describe("ProfileStatus component", () => {
  test("status from props should be in the state", () => {
    const component = create(<ProfileStatus status="it-kamsutra.com" />);
    const root = component.getInstance();
    expect(root.state.status).toBe.toString("it-kamasutra.com");
  });

  test("after creation <input> should be displayed", () => {
    const component = create(<ProfileStatus status="it-kamsutra.com" />);
    const root = component.root;
    let span = root.findByType("input");
    expect(span).not.toBeNull();
  });

  test("after creation <span> should contains correct status", () => {
    const component = create(<ProfileStatus status="it-kamsutra.com" />);
    const root = component.root;
    let span = root.findByType("span");
    expect(span.children[0]).toBe.toString("it-kamsutra.com");
  });
});