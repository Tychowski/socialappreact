import React from 'react'
import s from './profileinfo.module.css'
import Preloader from '../../common/Preloader/Preloader'
import ProfileStatus from './ProfileStatus'
import ProfileStatusWithHooks from './ProfileStatusWithHooks'

const ProfileInfo = (props) => {

    if (!props.profile) {
        return <Preloader />
    }

    return( 
        <div>
            <div className={s.contentCar}>
                <img src="https://www.calliaweb.co.uk/wp-content/uploads/2017/06/landscape-square-portrait-images-740x290.jpg"></img>
            </div>
            <div className={s.descriptionBlock}>
                <img src={props.profile.photos.large} />
                <ProfileStatusWithHooks status={props.status} updateStatus={props.updateStatus}/>
            </div>
        </div>
    )
}

export default ProfileInfo