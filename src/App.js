import React, { Component } from 'react'
import './App.css'
import HeaderContainer from './Components/Header/HeaderContainer'
import Navbar from './Components/Navbar/Navbar'
import DialogsContainer from './Components/Dialogs/DialogsContainer'
import { Route, BrowserRouter, withRouter } from 'react-router-dom'
import UsersContainer from './Components/Users/UsersContainer'
import ProfileContainer from './Components/Profile/ProfileContainer'
import Login from './Components/Login/Login'
import { initializedApp } from './Redux/app-reducer'
import { connect, Provider } from 'react-redux'
import Preloader from './Components/common/Preloader/Preloader'
import { compose } from 'redux'
import store from './Redux/redux-store'

class App extends Component {

  componentDidMount() {
    this.props.initializedApp()
  }

  render() {

    if (!this.props.initialized) {
      return <Preloader />
    }

    return(
      <BrowserRouter>
        <div className="app-wrapper">
          <HeaderContainer />
          <Navbar />
          <div className="app-wrapper-content">
            
            <Route path='/dialogs' render = { () => <DialogsContainer /> } />
            <Route path='/profile/:userId?' render = { () => <ProfileContainer /> } />
            <Route path='/users' render = { () => <UsersContainer /> } />
            <Route path='/login' render = { () => <Login /> } />

          </div>
        </div>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = (state) => ({
  initialized: state.app.initialized
})

let AppContainer = compose( 
  withRouter,
  connect(mapStateToProps, {initializedApp}))(App);


const SamuraiJSApp = (props) => {
  return <BrowserRouter>
    <Provider store={store}>
        <AppContainer state={store.getState()} dispatch={store.dispatch.bind(store)} store={store} />
    </Provider>    
  </BrowserRouter>
}

export default SamuraiJSApp;

