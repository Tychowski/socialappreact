import { usersAPI } from '../API/api'

const FOLLOW = 'FOLLOW';
const UNFOLLOW = 'UNFOLLOW';
const SET_USERS = 'SET_USERS';
const SET_CURRENT_PAGE = 'SET_CUREENT_PAGE';
const SET_TOTAL_USERS_COUNT = 'SET_TOTAL_USERS_COUNT';
const TOOGLE_IS_FETCHING = 'TOOGLE_IS_FETCHING';
const TOOGLE_IS_FOLLOWING_PROGRESS= 'TOOGLE_IS_FOLLOWING_PROGRESS';


const initialState = {
    users: [ ], 
    pageSize: 5,
    totalUserCount: 0,
    currentPage: 1,
    isFetching: true,
    followingInProgress: []
}

const usersReducer = (state = initialState, action) => {

    switch(action.type) {
        case FOLLOW:
            return {
                ...state, 
                users: state.users.map(u => {
                    if(u.id === action.userId) {
                        return {...u, followed: true}
                    }
                    return u;
                })
            }
            case UNFOLLOW:
                return {
                    ...state, 
                    users: state.users.map(u => {
                        if(u.id === action.userId) {
                            return {...u, followed: false}
                        }
                        return u;
                    })
                }
            case SET_USERS: 
                return { 
                    ...state, 
                    users: action.users
                }
            case SET_CURRENT_PAGE: 
                return { 
                    ...state, 
                    currentPage: action.currentPage
                }
            case SET_TOTAL_USERS_COUNT: 
                return { 
                    ...state, 
                    totalUserCount: action.count
                }
            case TOOGLE_IS_FETCHING: 
                return { 
                    ...state, 
                    isFetching: action.isFetching
                }
            case TOOGLE_IS_FOLLOWING_PROGRESS: 
                return { 
                    ...state, 
                    followingInProgress: action.isFetching 
                    ? [...state.followingInProgress, action.userId] 
                    : state.followingInProgress.filter(id => id != action.userId)
                }
            default: 
            return state;
    }
}

export const followSuccess = (userId) => ({ type: FOLLOW, userId })
export const unfollowSuccess = (userId) => ({ type: UNFOLLOW, userId })
export const setUsers = (users) => ({ type: SET_USERS, users })
export const setCurrentPage = (currentPage) => ({ type: SET_CURRENT_PAGE, currentPage })
export const setTotalUserCount = (totalUsersCount) => ({ type: SET_TOTAL_USERS_COUNT, count: totalUsersCount })
export const toggleIsFetching = (isFetching ) => ({ type: TOOGLE_IS_FETCHING, isFetching })
export const toggleFollowingProgress = (isFetching, userId ) => ({ type: TOOGLE_IS_FOLLOWING_PROGRESS, isFetching, userId })

// Sanka getUsers
export const getUsers = (currentPage, pageSize) => {
    return async (dispatch) => {
        dispatch(setCurrentPage(currentPage));
        dispatch(toggleIsFetching(true));
        let data = await usersAPI.getUsers(currentPage, pageSize)
        
        dispatch(toggleIsFetching(false));
        dispatch(setUsers(data.items));
        dispatch(setTotalUserCount(data.totalCount));
    }
}

export const follow = (userId) => {
    return async (dispatch) => {
        dispatch(toggleFollowingProgress(true, userId));
        let response = await usersAPI.follow(userId)
        if (response.data.resultCode == 0) {
            dispatch(followSuccess(userId));
        }
        dispatch(toggleFollowingProgress(false, userId));
    }
}

export const unfollow = (userId) => {
    return async (dispatch) => {
        dispatch(toggleFollowingProgress(true, userId));
        let response = await usersAPI.unfollow(userId)
        if (response.data.resultCode == 0) {
            dispatch(unfollowSuccess(userId));
        }
        dispatch(toggleFollowingProgress(false, userId));
    }
}


export default usersReducer