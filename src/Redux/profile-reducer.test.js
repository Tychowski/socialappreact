import profileReducer, { addPostActionCreator, deletePost } from './profile-reducer'

const state = {
    posts: [
        {id: 1, message: 'Hi, how are you ?', liksCount: 12},
        {id: 2, message: 'Its my first post', liksCount: 44},
        {id: 3, message: 'Its my first post', liksCount: 44}

    ]
}

test('new post shold be added', () => {
    // 1. test data
    let action = addPostActionCreator('test');

    // 2. action
    let newState = profileReducer(state, action)

    // 3. expectation
    expect(newState.posts.length).toBe(4);

});

test('new message shold be correct', () => {
    // 1. test data
    let action = addPostActionCreator('test');

    // 2. action
    let newState = profileReducer(state, action)

    // 3. expectation
    expect(newState.posts[3].message).toBe('test');

});

test('after eleting length of message should be decrement', () => {
    // 1. test data
    let action = deletePost(1);

    // 2. action
    let newState = profileReducer(state, action)

    // 3. expectation
    expect(newState.posts.length).toBe(2);

});

test('after eleting length of message should be decrement if id is incorrect', () => {
    // 1. test data
    let action = deletePost(1000);

    // 2. action
    let newState = profileReducer(state, action)

    // 3. expectation
    expect(newState.posts.length).toBe(3);

});