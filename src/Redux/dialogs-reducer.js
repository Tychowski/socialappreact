const UPDATE_NEW_MESSAGE_BODY = 'UPDATE_NEW_MESSAGE_BODY';
const SEND_MESSAGE = 'SEND_MESSAGE';

const initialState = {
    dialogs: [
        {id: 1, message: 'Hi'},
        {id: 2, message: 'Hello'},
        {id: 3, message: 'What is up'},
        {id: 4, message: 'Yo'},
        {id: 5, message: 'Yo'}
    ], 
    messages: [
        {id: 1, name: 'Ola'},
        {id: 2, name: 'Tom'},
        {id: 3, name: 'Jerry'},
        {id: 4, name: 'Anna'},
        {id: 5, name: 'Jan'}
    ]
}

const dialogsReducer = (state = initialState, action) => {

    let stateCopy;
    switch(action.type) {
        case UPDATE_NEW_MESSAGE_BODY:
            stateCopy = { 
                ...state,
                messages: [ ...state.messages ],
                newMessageBody: action.body
            }
            return stateCopy;
        case SEND_MESSAGE:
            let body = action.newMessageBody;
            stateCopy = { 
                ...state,
                messages: [ ...state.messages ]
            } 
            stateCopy.dialogs.push({id: 6, message: body})
            return stateCopy;
        default: 
            return state;
    }
}

export const sendMessageCreactor = (newMessageBody) => ({ type: SEND_MESSAGE, newMessageBody })
export const updateNewMessageBodyCreator = (body) => ({
    type: UPDATE_NEW_MESSAGE_BODY, 
    body: body
})

export default dialogsReducer